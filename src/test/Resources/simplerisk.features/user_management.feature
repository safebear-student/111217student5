Feature: User Management
  User Story: (optional)
  In order to manage users
  As an Administrator
  I need CRUD permissions to user accounts

  Rules:
  - Non-admin users have no permissions over other accounts
  - There are two types of non-admin accounts:
  -- Risk managers
  -- Asset managers
  - Non-admin users must only be able to see risks or assets in their Groups
  - The administrator must be able to reset user's passwords
  - When an account is created, a user must be forced to reset their password on first login

  Questions:
  - Do admin users need access to all accounts or only to the accounts in their Group?

  To do:
  - Force reset of password on account creation

  Domain Language:
  Group = Users are defined by which Group they belong to
  CRUD = Create, Read, Update, Delete
  administrator permissions = access to CRUD risks, users and assets for all groups
  risk_management permissions = Only able to CRUD risks
  asset_management permissions  = Only able to CRUD assets

  Background:
    Given a user called simon with administrator permissions and password S@afeB3ar
    And a user called tom with risk_management permissions and password S@feB3ar

  @high-impact
  Scenario Outline: The administrator checks a user's details
    When simon is logged in with password S@afeB3ar
    Then he is able to view <user>'s account
    Examples:
      | user |
      |tom   |

    @high-risk
    @to-do
  Scenario Outline: A user's password is reset by the administrator
    Given a <user> has lost his password S@feB3ar
    When simon is logged in with password S@feB3ar
    Then simon can reset <user>'s password
      Examples:
        | user |
        |tom   |




